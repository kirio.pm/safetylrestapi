package ues.edu.sv.safetylrestapi.filter;

public class TokenOnBlackListException extends RuntimeException {

    public TokenOnBlackListException() {
    }

    public TokenOnBlackListException(String message) {
        super(message);
    }
}
